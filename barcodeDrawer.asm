;~ Program draws in VGA mode, 320x200, 256 colors
;~ a barcode based on user input. Barcode is realized in CODE128 standard.

dane1 segment     
	input_buffer    db  24 dup(0)       ;bufor na tekst
	input_size      db  0               ;rozmiar tekstu
	x_coord			dw	0				;wspolrzedna X
	y_coord			dw	0				;wspolrzedna Y
	charSum         dw  104             ;suma do wyliczenia kontrolnej sumy
    controlSum      db  0               ;suma kontrolna
    cur_char        dw  0               ;aktualnie rysowany char
    code_stop       dw  1100011101011b  ;kod stopu
    code_size       db  0               ;ile bitow ma kod
    mask            dw  0               ;maska bitowa dla kodow
    len_error       db 'Za dlugi tekst. [ESC] $' ;blad dla za dlugiego wejscia
    input_error     db 'Brak argumentu. [ESC] $' ;blad dla braku wejscia    
    ascii_codes		dw	11011001100b 	;space  
                    dw  11001101100b    ;!	
                    dw  11001100110b    ;"	
                    dw  10010011000b    ;#
                    dw  10010001100b    ;$
                    dw  10001001100b    ;%
                    dw  10011001000b    ;&
                    dw  10011000100b    ;'
                    dw  10001100100b    ;(
                    dw  11001001000b    ;)
                    dw  11001000100b    ;*
                    dw  11000100100b    ;+
                    dw  10110011100b    ;,
                    dw  10011011100b    ;-
                    dw  10011001110b    ;.
                    dw  10111001100b    ;/
                    dw  10011101100b    ;0
                    dw  10011100110b    ;1
                    dw  11001110010b    ;2
                    dw  11001011100b    ;3
                    dw  11001001110b    ;4
                    dw  11011100100b    ;5
                    dw  11001110100b    ;6
                    dw  11101101110b    ;7
                    dw  11101001100b    ;8
                    dw  11100101100b    ;9
                    dw  11100100110b    ;:
                    dw  11101100100b    ;;
                    dw  11100110100b    ;<
                    dw  11100110010b    ;= 
                    dw  11011011000b    ;>
                    dw  11011000110b    ;?
                    dw  11000110110b    ;@
                    dw  10100011000b    ;A
                    dw  10001011000b    ;B
                    dw  10001000110b    ;C
                    dw  10110001000b    ;D
                    dw  10001101000b    ;E
                    dw  10001100010b    ;F
                    dw  11010001000b    ;G
                    dw  11000101000b    ;H
                    dw  11000100010b    ;I
                    dw  10110111000b    ;J
                    dw  10110001110b    ;K
                    dw  10001101110b    ;L
                    dw  10111011000b    ;M
                    dw  10111000110b    ;N
                    dw  10001110110b    ;O
                    dw  11101110110b    ;P
                    dw  11010001110b    ;Q
                    dw  11000101110b    ;R
                    dw  11011101000b    ;S
                    dw  11011100010b    ;T
                    dw  11011101110b    ;U
                    dw  11101011000b    ;V
                    dw  11101000110b    ;W
                    dw  11100010110b    ;X
                    dw  11101101000b    ;Y
                    dw  11101100010b    ;Z
                    dw  11100011010b    ;[
                    dw  11101111010b    ;\
                    dw  11001000010b    ;]
                    dw  11110001010b    ;^
                    dw  10100110000b    ;*
                    dw  10100001100b    ;_
                    dw  10010110000b    ;a`
                    dw  10010000110b    ;b
                    dw  10000101100b    ;c
                    dw  10000100110b    ;d
                    dw  10110010000b    ;e
                    dw  10110000100b    ;f
                    dw  10011010000b    ;g
                    dw  10011000010b    ;h
                    dw  10000110100b    ;i
                    dw  10000110010b    ;j
                    dw  11000010010b    ;k
                    dw  11001010000b    ;l
                    dw  11110111010b    ;m
                    dw  11000010100b    ;n
                    dw  10001111010b    ;o
                    dw  10100111100b    ;p
                    dw  10010111100b    ;q
                    dw  10010011110b    ;r
                    dw  10111100100b    ;s
                    dw  10011110100b    ;t
                    dw  10011110010b    ;u
                    dw  11110100100b    ;v
                    dw  11110010100b    ;w	
                    dw  11110010010b    ;x
                    dw  11011011110b    ;y
                    dw  11011110110b    ;z
                    dw  11110110110b    ;{
                    dw  10101111000b    ;|
                    dw  10100011110b    ;}
                    dw  10001011110b    ;~
                    dw  11010010000b    ;start

dane1 ends

stos1 segment stack
                    dw  255 dup(?)
    w_stosu			dw    ?
stos1 ends

code1 segment
    start:        
        mov ax, seg w_stosu             ;wczytuje stos
        mov ss, ax
        mov sp, offset w_stosu
        
        mov ax, seg input_buffer        ;wczytuje segment danych
        mov ds, ax
              
        mov ax, offset input_buffer
        call get_parameter   
        call calc_controlSum
        
        xor dx, dx
        mov dx, 0A000h
        mov es, dx                       ;do es segment pamieci graficznej
        mov al, 13h
        mov ah, 0h                       ;uruchamia tryb graficzny
        int 10h
        mov cx, 64000
        background:                      ;zamalowuje na bialo tlo
            mov di, cx
            dec di
            mov byte ptr es:[di], 15
            dec cx
            cmp cx, 0
            jne background
            
        call main_barcode
        call wyjscie
    
    
    ;main_barcode
    ;glowna procedura rysowania kodu
    main_barcode proc
        pusha
        mov ah, 0                        ;wyznaczam poczatek kodu
        mov al, byte ptr ds:[input_size] ;czyli wysrodkowanie w
        mov cl, 11                       ;zaleznosci od dlugosci
        mul cl                           ;frazy
        mov bx, 284
        xchg ax, bx
        sub ax, bx
        shr ax, 1
        mov word ptr ds:[x_coord], ax
        mov word ptr ds:[mask], 1024            ;maska do porownywania kodu znaku
        mov byte ptr ds:[code_size], 11         ;ktory jest 11-bitowy
        
        ;najpierw kod poczatku
        mov ax, word ptr ds:[ascii_codes+95*2]  ;kod start-b
        mov word ptr ds:[cur_char], ax          ;ustalam aktualny znak
        call draw_seg                           ;rysuje segment
        
        ;teraz kod kreskowy
        mov cx, 1
        
        barcode_loop:
            mov di, cx
            dec di
            mov dh, 0
            mov dl, byte ptr ds:[input_buffer+di] ;iteruje po buforze z tekstem
            mov bx, dx
            sub bx, 32                            ;odejmuje 32 by miec dobry indeks wartosci w tablicy
            shl bx, 1                             ;x2 bo tablica wordow
            
            mov ax, word ptr ds:[ascii_codes+bx]  ;kod znaku           
            mov word ptr ds:[cur_char], ax        ;ustalam aktualny znak 
            call draw_seg                         ;rysuje segment
            
            inc cx
            cmp cl, byte ptr ds:[input_size]      ;porownuje z dlugoscia tekstu
            jne barcode_loop
            
            
        ;suma kontrolna
        xor bx, bx
        mov bl, byte ptr ds:[controlSum]
        shl bx, 1
        mov ax, word ptr ds:[ascii_codes+bx]      ;kod od sumy kontrolnej            
        mov word ptr ds:[cur_char], ax            ;ustalam aktualny znak
        call draw_seg                             ;rysuje segment       
        
        ;kod konca        
        mov word ptr ds:[mask], 4096              ;trzeba zmienic maske
        mov byte ptr ds:[code_size], 13           ;bo kod 13-bitowy
        mov ax, word ptr ds:[code_stop]           ;kod znaku            
        mov word ptr ds:[cur_char], ax            ;ustalam aktualny znak
        call draw_seg                             ;rysuje segment       
             
        popa
        ret       
    main_barcode endp
    
    ;draw_seg
    ;rysuje segment danego znaku
    ;wejscie:
    ;x_coord - wsp. x
    ;cur_char - znak
    draw_seg proc
        pusha
        mov cl, byte ptr ds:[code_size]           ;pobiera dl. kodu
        mov bx, word ptr ds:[mask]                ;pobiera maske
        
        seg_loop:
            mov ax, word ptr ds:[cur_char]        ;pobiera aktualny znak
            and ax, bx                            ;i porownuje go z maska
            cmp ax, 0
            je is_zero                            ;jak 0 to nie rysujemy kolumny
            
            call draw_col
            
            is_zero:
                add word ptr ds:[x_coord], 1      ;przesuwam wsp. x o 1
                shr bx, 1                         ;dziele maske na 2
                dec cl                            ;iteracja mniej
                cmp cl, 0
                jne seg_loop    
        popa
        ret
    draw_seg endp
    
    ;draw_col
    ;rysuje pojedyncza kolumne
    draw_col proc
        pusha
        
        xor cx, cx
        
        column_loop:
            mov word ptr ds:[y_coord], cx         ;zeruje wsp. y
            call light_pixel                      ;wywoluje rys. pikselu
            inc cx
            cmp cx, 200                           ;rysuje w calej wys.
            jne column_loop
            
        popa
        ret
    draw_col endp
    
    ;light_pixel
    ;zapala piksel w danych wspolrzednych
	light_pixel proc
		pusha
		mov ax, 0A000h                  ;wczytuje segment pamieci obrazu
		mov es, ax
		mov ax, word ptr ds:[y_coord]   ;obliczam wartosci pamieci dla wspolrzednych
		mov di, ax
		shl di, 8                       ;y*256
		shl ax, 6                       ;y*64
		add di, ax                      ;y*320
		add di, word ptr ds:[x_coord]   ;320*y + x
		mov al, 0                       ;kolor czarny
		mov byte ptr es:[di], al        ;wczytuje piksel
		popa
		ret	
	light_pixel endp
	
	;calc_controlSum
	;wyznacza sume kontrolna kodu kreskowego
	calc_controlSum proc
	    pusha
	    xor si, si
	    mov bl, 1                       ;iterator
	    sum_loop:
	        mov dl, byte ptr ds:[input_buffer + si]   ;pobieram znak
	        sub dl, 32                  ;odejmuje 32 by miec wartosc zgodna z code128
	    
	        mov al, dl 	        
	        mul bl	                    ;mnoze przez indeks
	        
	        inc si	        
	        
	        add ax, word ptr ds:[charSum]
	        mov word ptr ds:[charSum], ax    ;dodaje do ogolnej sumy
	        
	        inc bl
	        cmp bl, byte ptr ds:[input_size]
	        jne sum_loop
	        
	    mov bl, 103                    ;podzial przez 103 zgodnie z code128
	    div bl
	       
	    mov byte ptr ds:[controlSum], ah   ;zapamietuje reszte z dzielenia
	        
	    popa
	    ret
	calc_controlSum endp
    ;get_parameter
    ;pobiera parametr z linii komend
    ;wejscie:
    ;ax - bufor na tekst
    get_parameter proc
        pusha
        mov bx, ax                      ;kopia offsetu buforu
        xor cx, cx                      ;zeruje counter
        mov cl, es:[80h]                ;zczytuje dlugosc argumentu
        cmp cl, 1                       ;sprawdzam czy w ogole jest
        jna no_input_error
        cmp cl, 25                      ;sprawdzam czy nie za dlugi input
        ja  len1_error
        mov input_size, cl              ;zapamietuje rozmiar
        xor di, di
        xor si, si
        mov di, 82h
        argument_loop:
            mov al, byte ptr es:[di]    ;pobieram kolejny znak argumentu
            
            mov byte ptr ds:[bx + si], al   ;wpisuje kolejny znak argumentu do bufora
            
            inc di                      ;przesuwam wskazniki
            inc si
            dec cl                      ;zmiejszam iterator
            cmp cl, 1
            jne argument_loop
        popa
        ret
        no_input_error:                 ;obsluga bledow
            mov ah, 9
            lea dx, input_error
            int 21h
            call wyjscie
        len1_error:
            mov ah, 9
            lea dx, len_error
            int 21h
            call wyjscie          
    get_parameter endp 

	;wyjscie z programu
	wyjscie proc                       ;wyjscie czeka na esc
	    xor bx, bx
	    exit_loop:
	        mov ah, 1
	        int 21h 
	        cmp al, 1Bh
	        jne exit_loop
	    mov ax, 3h
	    int 10h                        ;gasze tryb graficzny
		mov ah, 4ch                    ;a nastepnie program
		int 21h
    wyjscie endp
code1 ends
    
end start
