;~ Program counts the amount of various bytes (characters) in input file.
;~ And writes their counts to output file


dane1 segment
    input_buffer    db  ?                   ;bufor na tekst
    filename1       db  30 dup(0), 0        ;nazwa pliku odczyt
    filename2       db  30 dup(0), 0        ;nazwa pliku na zapis
    file2           db  "plik1.txt",0
    len_error       db 'Za dlugi tekst. [ESC] $' ;blad dla za dlugiego wejscia
    input_error     db 'Brak argumentu. [ESC] $' ;blad dla braku wejscia
    handle          dw  ?                   ;handler
    counters        dw  255 dup(0)          ;liczniki znakow
dane1 ends

stos1 segment stack
                    dw  255 dup(?)
    w_stosu			dw    ?
stos1 ends

code1 segment
    start:        
        mov ax, seg w_stosu             ;wczytuje stos
        mov ss, ax
        mov sp, offset w_stosu
        
        mov ax, seg input_buffer        ;wczytuje segment danych
        mov ds, ax
        
        call get_files
        
        call open_file
        call read_file
        call close_file
        
        call write_file
        
        call wyjscie
        
        
    open_file proc
        pusha
        mov ah, 03dh
        lea dx, filename1
        mov al, 0
        int 21h
        jc open_err
        mov word ptr ds:[handle], ax
        popa
        ret
        open_err:
        lea dx, input_error
        mov ah, 9
        int 21h
        popa
        ret
    open_file endp
    
    read_file proc
        read_loop:
        mov ah, 03fh
        mov bx, word ptr ds:[handle]
        lea dx, input_buffer
        mov cx, 1
        int 21h
        cmp ax, 0
        jz eof
        mov dl, byte ptr ds:[input_buffer]
        cmp dl, 1ah
        jz eof
        mov bx, dx
        shl bx, 1
        inc word ptr ds:[counters + bx]
        ;mov ah, 2
        ;int 21h
        jmp read_loop
        eof:
            ret        
    read_file endp
    
    close_file proc
        mov ah, 3eh
        mov bx, word ptr ds:[handle]
        int 21h
        ret        
    close_file endp
    
    write_file proc
        pusha
        mov ah, 03ch
        lea dx, filename2
        mov cx, 0
        int 21h
        jc open_err
        mov word ptr ds:[handle], ax
        
        ;write to
        mov cx, 0
        write_loop:
        mov di, cx
        push cx
        mov dx, 1ah
        mov cx, 2
        mov bx, word ptr ds:[handle]
        mov ah, 40h
        int 21h
        
        mov dx, 0Dh
        mov cx, 1
        mov ah, 40h
        int 21h
        pop cx
        inc cx
        cmp cx, 5
        jne write_loop 
        popa
        ret        
    write_file endp
    
    ;get_files   
    ;pobiera nazwy plikow z linii komend  
    get_files proc    
        pusha    
        mov bx, offset filename1        ;kopia offsetu buforu    
        xor cx, cx                      ;zeruje counter    
        mov cl, es:[80h]                ;zczytuje dlugosc argumentu    
        cmp cl, 1                       ;sprawdzam czy w ogole jest    
        jna no_input_error    
        cmp cl, 120                      ;sprawdzam czy nie za dlugi input    
        ja  len1_error    
        ;mov input_size, cl              ;zapamietuje rozmiar    
        xor di, di    
        xor si, si    
        mov di, 82h    
        file1_loop:    
            mov al, byte ptr es:[di]    ;pobieram kolejny znak argumentu    
            cmp al, 20h
            je file1_loop_out        
            mov byte ptr ds:[bx + si], al   ;wpisuje kolejny znak argumentu do bufora
            
            inc di                      ;przesuwam wskazniki
            inc si
            dec cl                      ;zmiejszam iterator
            cmp al, 20h
            jne file1_loop
            file1_loop_out:
                inc di                      ;przesuwam wskazniki
                dec cl                      ;zmiejszam iterator
                xor si, si
                mov bx, offset filename2
                jmp file2_loop
        file2_loop:    
            mov al, byte ptr es:[di]    ;pobieram kolejny znak argumentu    
                    
            mov byte ptr ds:[bx + si], al   ;wpisuje kolejny znak argumentu do bufora
            
            inc di                      ;przesuwam wskazniki
            inc si
            dec cl                      ;zmiejszam iterator
            cmp cl, 1
            jne file2_loop    
        popa
        ret
        no_input_error:                 ;obsluga bledow
            mov ah, 9
            lea dx, input_error
            int 21h
            call wyjscie
        len1_error:
            mov ah, 9
            lea dx, len_error
            int 21h
            call wyjscie          
    get_files endp 

	;wypisz liczbe
	;wejscie:
	;ax - liczba do wypisania
	print_number proc
	    pusha
	    mov bx, 10
	    xor cx, cx
	    stack_loop:
	        xor dx, dx                ;dzielenie liczby przez 10
	        div bx                    ;i wypisywanie kolejnych
	        push dx                   ;cyfr
	        inc cx
	        cmp ax,0	        
	        jne stack_loop
	        
	    print_loop:
	        pop dx
	        add dx, '0'
	        mov ah, 2
	        int 21h
	        dec cx
	        cmp cx, 0
	        jne print_loop
	        
	    popa
	    ret
	print_number endp
    
    

	;wyjscie z programu
	wyjscie proc                       ;wyjscie czeka na esc
	    xor bx, bx
	    exit_loop:
	        mov ah, 1
	        int 21h 
	        cmp al, 1Bh
	        jne exit_loop
		mov ah, 4ch                    ;a nastepnie program
		int 21h
    wyjscie endp  
	
code1 ends

end start
