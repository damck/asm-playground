;Description:
;Program plots a function given by:
;y=ax+b
;
;~ After execution program asks for a and b parametrs
;~ after which it displays the X for which the function
;~ has value 0. a and b are signed, with one point accuracy, ie. 1.5
;~ Afterwards, the program displays (in text) a simplified plot.

dane1 segment 
	a_buff          db 6 dup(0)
	a_len           dw 0
	b_buff          db 6 dup(0)	
	b_len           dw 0
	a               dw 0
	a_sign          db 0
	a_dot           db 0
	b               dw 0
	b_sign          db 0
	b_dot           db 0
	dot_flag        db 0 
	root            dw 0
	root_rem        dw 0
	root_sign       db 0
	f_const         db 0
	scale_x         dw 0
	scale_y         dw 0
	x_point         db 0
	y_point         db 0
	less_points     dw 0
	text_a	        db 'Prosze podac wspolczynnik a: $'
    text_b	        db 'Prosze podac wspolczynnik b: $'
	parse_err       db 'Bledne dane $' 
	root_text       db 'Wartosc miejsca zerowego: $'
	no_root_text    db 'Brak miejsca zerowego. $'
	wrong_fun_text  db 'Nieskonczenie wiele miejsc zerowych. $'
	scale_y_text    db 'Skala Y - 1:$'
	scale_x_text    db '  Skala X - 1:$'
	close_text      db 'Ukonczono, nacisnij [ENTER] by wyjsc $'
	left_down       dw 0F00h
	left_up         dw 0280h
	middle          dw 0976h
	space           db ' '
	hash            db '#'
dane1 ends

stos1 segment stack
                    dw  255 dup(?)
    w_stosu			dw    ?
stos1 ends

assume ds:dane1

kod1 segment

	start:
		mov ax, seg w_stosu
		mov ss, ax
		mov sp, offset w_stosu
		
		mov ax, seg a_buff
		mov ds, ax
	    
	    mov ax,3			; tryb tekstowy 80x25 znak�w
		int 10h				; przejscie w tryb tekstowy
		
		mov ax,0b800h       ;ustawienie bufora wideo
		mov es,ax			;na dodatkowy segment danych
	    
	    mov ah, 2
	    mov bh, 0
	    mov dx, 0
	    int 10h
	    
	    mov ah, 9            ;wczytanie wsp. a
	    lea dx, text_a
	    int 21h
	    
	    lea ax, a_buff
	    lea bx, a

	    call read_num
	    call parse_num
	    
	    call print_enter
	    
	    mov ah, 9            ;wczytanie wsp. b
	    lea dx, text_b
	    int 21h
	    
	    lea ax, b_buff
	    lea bx, b
	    call read_num
	    call parse_num
	    
	    call print_enter

	    call print_root      ;wypisanie wartosci m. zerowego
	    
	    call print_enter
	    
	    
	    mov bx, 0280h        ;narysowanie osi
        call draw_halfy
        mov bx, 08C0h        
        call draw_x
        mov bx, 0960h        
        call draw_halfy
        
        call main_plot       ;rysowanie funkcji wlasciwej

	
	;rysuje polowke osi y
	;bx - pozycja startowa    
	draw_halfy proc
	    mov cx, 10
	    loop1_1:
	        push cx
	        mov cx, 9	        
	        mov al, byte ptr ds:[space]
	        mov ah, 15	        
	        loop1_2:
	            mov word ptr es:[bx], ax    ;wypisz spacje	                  	       
	            add bx, 2
	            dec cx
	            cmp cx, 0
	            jne loop1_2
	        pop cx
	        dec cx
	        add bx, 2
	        mov al, byte ptr ds:[hash]  ;wypisz element osi
	        mov word ptr es:[bx], ax
	        add bx, 160                 ;przesun o linije
	        sub bx, 20                  ;cofnij o przesuniecie
	        cmp cx, 0
	        jne loop1_1 	    
	    ret
	draw_halfy endp
	
	;rysuj os x
	;bx - pozycja startowa 
	draw_x proc
	    mov al, ds:[hash]
	    mov ah, 15
	    mov cx, 21
	    loop2:
	        mov es:[bx], ax	                  	       
	        add bx, 2
	        dec cx
	        cmp cx, 0
	        jne loop2
	    ret
	draw_x endp
	
	;wczytaj liczbe jako "string"
	;wejscie: ax - bufor na string
    read_num proc
        pusha
        mov bx, ax          ;kopia offsetu poczatku buforu
        xor di, di
        mov cx, 6           ;chcemy max 6 znakow w buforze
           
	    main_loop:
	        push ax
	        push cx
	        mov ah, 3       ;pobierz wspolrzedne kursora
	        int 10h
	        pop cx
	        pop ax
	        
	        mov ah, 1
	        int 21h
            cmp al, 13d     ;czy enter
            je enter_handler
            cmp al, 8d      ;czy backspace
            je backspace_handler
            cmp cx, 0       ;czy mozna jeszcze dopisywac            
            jne enter_char
            je no_char
            
            push bx
			mov ah,2
			mov bh,0
			int 10h 
			pop bx
			jmp main_loop
			
        no_char:
            push bx
            mov ah, 2       ;wroc do poprzedniej pozycji po tym
            mov bh, 0       ;jak int21h przesunal kursor
            int 10h
            pop bx
            push dx
            mov ah, 2       ;wpisz null w pozycji
            mov dl, 0
            int 21h
            pop dx
            push bx
            mov ah, 2       ;wroc do poprzedniej pozycji po tym
            mov bh, 0       ;jak int21h przesunal kursor
            int 10h
            pop bx
            jmp main_loop  
            
        backspace_handler:
            cmp cx, 6
            je no_char
            delete:
                dec dl          ;cofam sie o kolumne
                dec di          ;cofam wskaznik w buforze
                inc cx
                mov byte ptr ds:[bx + di], 0    ;zeruje wartosc w buforze
                jmp no_char
                        
        enter_char:
            dec cx
            mov byte ptr ds:[bx + di], al
            inc dl          ;przesuwam kolumne do przodu
            inc di          ;przesuwam wskaznik w buforze
            jmp main_loop
            
        enter_handler:           
            neg cx
            add cx, 6
            mov word ptr ds:[bx + 6], cx    ;zapamietaj dlugosc inputu
            popa
            ret
    read_num endp	
	
	;przetlumacz string na liczbe
	;wejscie:
	;ax - offset bufora
	;bx - offset miejsca na liczbe
	parse_num proc
	        pusha
	        
	        push bx
	        mov bx, ax
	        mov dl, byte ptr ds:[bx]
	        mov cx, word ptr ds:[bx+6]      ;pobieram dlugosc wspolczynnika    	    
	        mov byte ptr ds:[dot_flag], 0
	        pop bx
	        
	        
	        cmp dl, '-'     ;sprawdzam czy pierwszy znak to minus
	        je has_minus
	        jmp parse
	        
	        has_minus:
	            mov byte btr ds:[bx+2], 1       ;offset dwa od wskazania na liczbe to wartosc ?_sign
                dec cx
                inc ax
                
            parse:
                xor dx, dx
                xor si, si
                    parse_loop:
                        push bx
                        mov bx, ax
                        mov dl, byte ptr ds:[bx+si]
                        cmp dl, '.'
                        je has_dot
                        cmp dl, '0'            ;sprawdzam czy to w ogole liczba
                        jb not_num
                        cmp dl, '9'
                        ja not_num
                        sub dl, 030h           ;odejmuje od wartosci ascii wartosc '0'
                        pop bx
                        
                        push ax
                        mov ax, word ptr ds:[bx]
                        
                        push bx             ;mnozenie przez 10
                        mov bx, ax          ;dotychczasowej wartosci
                        shl ax, 1
                        shl bx, 3
                        add ax, bx
                        pop bx
                        
                        add ax, dx
                        jc too_big
                        mov word ptr ds:[bx], ax
                        pop ax
                        
                        inc si
                        dec cx
                        cmp cx, 0
                        jne parse_loop
                        je exit
                    
                    has_dot:
                        pop bx
                        cmp dot_flag, 0
                        jne not_num
                        mov byte ptr ds:[dot_flag], 1
                        mov byte ptr ds:[bx+3], 1
                        cmp cx, 1
                        jbe not_num
                        mov cx, 1
                        inc si
                        jmp parse_loop
                    too_big:
                        call print_enter
                        lea dx, parse_err
                        mov ah, 09h
                        int 21h
                        call wyjscie     
                    not_num:
                        call print_enter
                        lea dx, parse_err
                        mov ah, 09h
                        int 21h
                        call wyjscie    
                    exit:
                        popa
                        ret                       
                
	parse_num endp    
	
	
	;oblicz wartosc miejsca zerowego
	get_root proc
	    pusha
	    mov ax, word ptr ds:[b]
	    mov bx, word ptr ds:[a]
	    xor dx, dx 
	    push dx
	    mov dh, byte ptr ds:[b_dot]
	    mov dl, byte ptr ds:[a_dot]
	    cmp dh, dl	    
	    je same_precion
	    jb b_dott
	    ;albo a_dott
	        push ax             ;mnozenie przez 10
            mov ax, bx          ;dotychczasowej wartosci
            shl ax, 1
            shl bx, 3
            add bx, ax
            pop ax
            jmp same_precion
	    b_dott: 
	        push bx             ;mnozenie przez 10
            mov bx, ax          ;dotychczasowej wartosci
            shl ax, 1
            shl bx, 3
            add ax, bx
            pop bx
            	    
        same_precion:
        pop dx	    
	    div bx
	    
	    mov word ptr[root], ax
	    
	    mov ax, dx
	    xor dx, dx
	    
	    push bx
	    mov bx, 100
	    mul bx
	    pop bx
	    div bx
	    
	    mov word ptr ds:[root_rem], ax
	    
	    mov dl, byte ptr ds:[a_sign]        ;wyznacz znak
	    mov dh, byte ptr ds:[b_sign]        ;miejsca zerowego
	    xor dl, dh
	    xor dl, 1
	    mov byte ptr ds:[root_sign], dl
	    
	    popa
	    ret
	get_root endp
	
	;przejdz do nowej linii
	print_enter proc
	    pusha
	    mov ah, 03h
	    xor bx, bx
	    int 10h
	    
	    inc dh
	    xor dl,dl
	    mov ah, 02h
	    int 10h
	    popa
	    ret
	print_enter endp
	
	;wypisuje miejsce zerowe
	print_root proc
	    pusha
	    lea dx, root_text
	    mov ah, 9
	    int 21h
	    
	    mov ah, 03h
	    int 10h
	    add dl, 1
	    push bx
	    xor bx, bx
	    mov ah, 02h
	    int 10h
	    pop bx 
	    
	    cmp word ptr ds:[a], 0          ;test czy nie ma 0
	    je no_root
	    jmp print	    
	    no_root:
	        cmp word ptr ds:[b], 0
	        je wrong_fun	        	        
	        jmp no_root1
		    wrong_fun:
		        lea dx, wrong_fun_text
		        mov ah, 9
	            int 21h
	            mov byte ptr ds:[f_const], 1
	            jmp exit_print_root
		    no_root1:
		        lea dx, no_root_text
		        mov ah, 9
	            int 21h
	            mov byte ptr ds:[f_const], 1
	            jmp exit_print_root
   
	    print:
	        call get_root
	        mov ah, byte ptr ds:[root_sign] ;sprawdz czy miejsce
	        cmp ah, 1                       ;zerowe ma znak '-'
	        jne no_minus
	        mov dl, '-'
	        mov ah, 02h
	        int 21h
	        no_minus:
    	        mov ax, word ptr ds:[root]    ;wypisz glowna czesc
    	        call print_number
    	        mov dl, ','                   ;dodaj przecniek
    	        mov ah, 02h
    	        int 21h
    	        mov ax, word ptr ds:[root_rem]
    	        cmp ax, 9
    	        ja no_zero	                  ;jezeli wartosc po
    	        mov dl, '0'                   ;przecinku to cyfra
    	        push ax                       ;to dopisz '0'
    	        mov ah, 02h
    	        int 21h
    	        pop ax
    	        no_zero:
    	            call print_number         ;w innym wypadku wypisz
	    exit_print_root:
	        popa
	        ret
	print_root endp
	
	;wypisz liczbe
	;wejscie:
	;ax - liczba do wypisania
	print_number proc
	    pusha
	    mov bx, 10
	    xor cx, cx
	    stack_loop:
	        xor dx, dx                ;dzielenie liczby przez 10
	        div bx                    ;i wypisywanie kolejnych
	        push dx                   ;cyfr
	        inc cx
	        cmp ax,0	        
	        jne stack_loop
	        
	    print_loop:
	        pop dx
	        add dx, '0'
	        mov ah, 2
	        int 21h
	        dec cx
	        cmp cx, 0
	        jne print_loop
	        
	    popa
	    ret
	print_number endp
	
	;glowna funkcja od wykresu
	main_plot proc
	    pusha
	    mov ah, byte ptr ds:[f_const]   ;sprawdzam czy funkcja
	    cmp ah, 1                       ;nie jest stala
	    jne not_const
	    call draw_const
	    not_const:
	        call draw_fun
	    popa
	    ret
	main_plot endp
	
	;rysuje funkcje liniowa
	draw_fun proc
	    pusha          
	    
	    call get_scale_x
	    call get_scale_y                ;wyliczam skale
	    
	    push ax
	    
	    lea dx, scale_y_text            ;wypisuje skale osi y
	    mov ah, 9
	    int 21h
	    mov ax, word ptr ds:[scale_y]	    
	    call print_number	    
	    
	    lea dx, scale_x_text            ;wypisuje skale osi x
	    mov ah, 9
	    int 21h
	    mov cx, word ptr ds:[scale_x]
	    mov ax, 1
	    mov bx, 10
	    cmp cx, 0
	    je scale_1
	    scale_x_loop:
            mul bx
            dec cx
            cmp cx, 0
            jne scale_x_loop
        scale_1:    
	        call print_number
	    
	    mov al, byte ptr ds:[x_point]      ;offset miejsca zerowego
	    push ax
	    mov al, byte ptr ds:[a_sign]       ;sprawdz czy funkcja
	    cmp al, 0                          ;rosnaca czy malejaca
	    je rising
	        mov bx, word ptr ds:[left_up]      ;w zaleznosci od tego
	        mov dx, -160                       ;gdzie indziej zacznij
	        jmp chosen
	    rising:
	        mov bx, word ptr ds:[left_down]    ;punkt odniesienia
	        mov dx, 160
	    chosen:
	    pop ax
	    call calc_start_point_fun          ;wyznacz punkt startowy na wykresie
	    
	    mov cx, 21              ;21 punktow jest ogolem na osi x
	    sub cx, word ptr ds:[less_points]   ;odejmuje "stracone" punkty
	    mov ah, 03h             ;wyglad funkcji
	    mov al, '*'
	    draw_fun_loop:	                 ;rysowanie funkcji
	        mov word ptr es:[bx], ax
	        dec cx
	        sub bx, dx
	        add bx, 2
	        cmp cx, 0
	        jne draw_fun_loop
	        
	    call wyjscie	    
	draw_fun endp
	
	;rysuje funkcje stala
	draw_const proc
	    pusha
	    call get_scale_const
	    push ax
	    lea dx, scale_y_text
	    mov ah, 9
	    int 21h
	    mov ax, word ptr ds:[scale_y]	    
	    call print_number
	    
	    lea dx, scale_x_text
	    mov ah, 9
	    int 21h
	    mov ax, word ptr ds:[scale_x]	    
	    call print_number
	    
	    mov al, -10                        ;wspolrzedna x
	    mov ah, byte ptr ds:[y_point]      ;wspolrzedna y
	    mov bx, word ptr ds:[left_down]    ;punkt odniesienia
	    call calc_start_point_cnst         ;wyznacz punkt startowy na wykresie
	    
	    mov cx, 21              ;wiem ze 21 punktow bo stala
	    mov ah, 03h             ;wyglad funkcji
	    mov al, '*'
	    draw_cst_loop:	    
	        mov word ptr es:[bx], ax
	        dec cx
	        add bx, 2
	        cmp cx, 0
	        jne draw_cst_loop
	        
	    call wyjscie	    
	draw_const endp
    
    ;oblicza skale dla funkcji stalej
    get_scale_const proc
        pusha
        
        mov cx, 1       ;w cx wartosc skali
        mov ax, word ptr ds:[b]
        mov bx, 10      ;skalujemy dzielac/mnozac przez 10
        
        scale_1_loop:
            cmp ax,10
            jle break_scale_1_loop
            xor dx, dx  ;usuwam reszte
            div bx
            push ax
            mov ax, cx
            xor dx, dx
            mul bx
            mov cx, ax
            pop ax
            jmp scale_1_loop
            
       break_scale_1_loop:
            mov word ptr ds:[scale_y], cx
            mov word ptr ds:[scale_x], 1
            cmp byte ptr ds:[b_sign], 0
            je exit_scale_1
                neg al
            exit_scale_1:
                mov byte ptr ds:[y_point], al
                popa
                ret
    get_scale_const endp
    
    
    ;oblicza skale y dla funkcji liniowej    
    get_scale_y proc
		pusha
		mov ax, word ptr ds:[x_point]
		xor ah,ah
		cmp al,0
		jge scale_2
			neg al			; w al mamy teraz wartosc bezwzgledna
	    scale_2:
		    cmp al,0
		    jne scale_2_2
			mov al,1		; zapobiegamy dzieleniu przez 0
    	scale_2_2:
    		mov bx,ax			; bedziemy dzielic przez bx
    		mov ax,word ptr ds:[b]
    		xor dx,dx
    		div bx
    		cmp ax,0
    		jne not_zero
    			mov ax,1
    	not_zero:
    		mov word ptr ds:[scale_y],ax    		
    		popa
    		ret
    get_scale_y endp
    
    ;oblicza skale x dla funkcji liniowej
    get_scale_x proc
        pusha
        
        xor cx, cx      ;w cx wartosc skali
        mov ax, word ptr ds:[root]
        mov bx, 10      ;skalujemy dzielac/mnozac przez 10
        
        scale_3_loop:
            cmp ax, 10
            jle break_scale_3_loop
            xor dx, dx  ;usuwam reszte
            div bx
            inc cx
            jmp scale_3_loop
            
        break_scale_3_loop:
            mov word ptr ds:[scale_x], cx
            cmp byte ptr ds:[root_sign], 0
            je exit_scale_3
                neg al
            exit_scale_3:
                mov byte ptr ds:[x_point], al
                popa
                ret
    get_scale_x endp
    
    ;wylicza punkt rozpoczecia na wykresie dla f. stalej
    ;wejscie:
    ;al - offset x
    ;ah - offset y
    ;bx - generalny offset
    ;wyjscie:
    ;bx - wspolrzedne punktu startowego dla bufora wideo
    calc_start_point_cnst proc
        add al, 10
        cmp al, 0
        je check_y
        loop_x:         ;najpierw ile w prawo
            add bx, 2   ;zasadniczo to 0
            dec al
            cmp al, 0
            ja loop_x
        check_y:
        add ah, 10
        cmp ah, 0 
        je exit_calc
        loop_y:         ;teraz gora
            sub bx, 160
            dec ah
            cmp ah, 0
            ja loop_y
        exit_calc:
            ret
    calc_start_point_cnst endp

    ;wylicza punkt rozpoczecia na wykresie dla f. liniowej
    ;wejscie:
    ;al - offset miejsca zerowego
    ;bx - generalny offset
    ;wyjscie:
    ;bx - wsp. punktu startowego dla bufora wideo    
    calc_start_point_fun proc
        push cx
        xor cx, cx              ;w cx licze ile punktow
        cmp al, 0               ;"przepada" przez przesuniecie
        je exit_calc_2
        jg mov_x
        cmp byte ptr ds:[a_sign], 0
        je mov_y_up   
            mov_y_down:         ;ile w dol, dla malejacej
                inc cx             
                add bx, 160
                inc al          ;zasadniczo jak root < 0
                cmp al, 0       ;to idziemy gora/dol
                jl mov_y_down
                jmp exit_calc_2                
            mov_y_up:           ;ile w gore, dla rosnacej
                inc cx          
                sub bx, 160
                inc al
                cmp al, 0
                jl mov_y_up
                jmp exit_calc_2                        
        mov_x:                  ;ile w prawo
            inc cx
            add bx, 2           ;a jak root > 0 to w prawo
            dec al
            cmp al, 0
            jg mov_x
        exit_calc_2:
            mov word ptr ds:[less_points], cx   ;zapamietuje il.
            pop cx                              ;punktow
            ret
    calc_start_point_fun endp
    
    
    ;wyjscie z programu
	wyjscie proc                       ;wyjscie czeka na enter
	    xor bx, bx
	    mov dx, 101Eh
	    mov ah, 2
	    int 10h
	    lea dx, close_text
	    mov ah, 9
	    int 21h
	    exit_loop:
	        mov ah, 1
	        int 21h 
	        cmp al, 0Dh
	        jne exit_loop
		mov ah, 4ch
		int 21h
    wyjscie endp
	
kod1 ends
end start
